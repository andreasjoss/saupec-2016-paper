#!/usr/bin/python
import argparse
import os
 
parser = argparse.ArgumentParser(description="compile",formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument('--file')

args = parser.parse_args()

if args.file.endswith('.pic'):
  args.file = args.file[:-4]
    
  string1 = "dpic -p %s"%(args.file)
  string2 = ".pic > %s"%(args.file)
  final = string1 + string2 + ".tex"
  #print(final)

  #print("dpic -p %s"%(args.file))
  print("xxxxxxxxxxxxxxxxxxxxxx")
  print("pic to tex")
  os.system(final)							#create the tex file from the pic commands
  print("xxxxxxxxxxxxxxxxxxxxxx")
  print("tex to dvi")  
  os.system("latex %s.tex"%(args.file)) 				#create the dvi file from the tex commands
  print("xxxxxxxxxxxxxxxxxxxxxx")
  print("dvi to ps")  
  os.system("dvips -Ppdf -G0 %s.dvi"%(args.file))			#dvi to ps
  print("xxxxxxxxxxxxxxxxxxxxxx")
  print("ps to pdf")  
  os.system("ps2pdf %s.ps"%(args.file))    				#ps to pdf
  print("finished")
else:
  print "Andreas code: Incorrect source file was given"