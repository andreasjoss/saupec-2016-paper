\documentclass[10pt]{SAIEETranAFR05}

%\usepackage{lmodern}
\usepackage[cmex10]{amsmath}
\usepackage{graphicx}
\usepackage{IEEEtrantools}
\usepackage{cite}
\usepackage[tight,footnotesize]{subfigure}
\usepackage{multirow}
% \usepackage{pdftricks}
\usepackage{pstricks}
% \usepackage{pst-pdf}
\usepackage{array}
\usepackage{tikz}
\usepackage{siunitx}
\usetikzlibrary{shapes.geometric, arrows}

% \begin{psinputs}
%    \usepackage{pstricks}
%    \usepackage{multido}
% \end{psinputs}

\newcolumntype{x}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}p{#1}}

\title{DESIGN AND OPTIMISATION OF AN IRONLESS DOUBLE-ROTOR RADIAL FLUX PERMANENT MAGNET MACHINE.}

\author{A. Joss
         and 
        PJ. Randewijk
        \thanks{Department of Electrical and Electronic Engineering, Stellenbosch University, South Africa
        \mbox{E-mail: 16450612@sun.ac.za}}}

\abstract{This paper consists of the multi-objective optimisation of an Ironless Double-Rotor Radial Flux Permanent Magnet (IDRFPM) machine. The electromagnetic aspects are mainly considered. A methodology regarding the multi-objective optimisation of the IDRFPM machine is presented. The objectives to be optimised include efficiency, torque and PM mass. The solutions of the optimisation process are also briefly discussed. Furthermore, eddy current losses due to the magnetic flux experienced by the coil windings are investigated.}

\keywords{multi-objective optimisation, permanent magnet electrical machines , eddy current losses}

\newcommand{\semfem}{\textit{SEMFEM}}


\begin{document}
  \bstctlcite{IEEE:BSTcontrol}
  \maketitle
  
  \section{INTRODUCTION}
  This paper consists of the multi-objective optimisation of an Ironless Double-Rotor Radial Flux Permanent Magnet (IDRFPM) machine. The electromagnetic aspects of the machine are considered in the optimisation process. The magnets which are to be optimised are arranged in a quasi-Halbach topology. According to the authors' knowledge, no literature exists which comprehensively discusses the design optimisation of this machine. A methodology regarding the multi-objective optimisation of the IDRFPM machine is presented. The objectives to be optimised include efficiency, torque and PM mass. The results of the optimisation process are also discussed. A prototype machine exists, which was used as a benchmark for other possible solutions. The prototype machine was designed using only engineering intuition. For this paper, it is desired to obtain an improved machine design. Furthermore, eddy current losses due to the magnetic flux experienced by the coil windings are investigated, and the eddy current losses of the prototype machine are measured and compared to theoretical values. 
  
  \section{GOAL}
  The goal of this study is to optimise the IDRFPM machine with regard to three competing objectives, namely efficiency ($\eta$), torque ($\tau$) and PM mass ($m$). The existing prototype machine is used as a benchmark for the evaluation of new design solutions. The prototype machine is designed in \cite{Oosthuizen2015a} for a prototype car that would be entered in the Shell Eco Marathon competition. 
  
  The three competing objectives give rise to the term ``Pareto optimal set''. The aforementioned term is a set of solutions non of which are superior to the other. A solution is Pareto optimal if it is not possible to improve at least one objective function without causing another objective function to deteriorate.  In this paper, the decision-maker is presented with a set of Pareto optimal solutions a posteriori. This means the decision-maker does not articulate preferences beforehand, but rather manually selects a solution after completion of an approximate Pareto front. This way, amongst other things, the decision-maker can decide between similarly pleasing results and compare designs according to ease of manufacturing. Furthermore, it is possible for similar solutions in the objective space, $\textbf{f}(\textbf{x})$, to correspond with completely different solutions in the design space, \textbf{x}.
  
  In general, for any multi-objective function, it is desired to optimise a vector of objective functions
  \begin{equation}\label{eq:1}
  \text{Optimise  } \textbf{f}(\textbf{x}) = [f_{1}(\textbf{x}), f_{2}(\textbf{x}), ...,f_{k}(\textbf{x})]^{T}
  \end{equation}
  In this paper, the vector of multi-objective functions is described as
  \begin{equation}\label{eq:2}
  \text{Optimise  } \textbf{f}(\textbf{x}) = [f_{\eta}(\textbf{x}), f_{\tau}(\textbf{x}), f_{m}(\textbf{x})]^{T}
  \end{equation}
  where $\textbf{x}$ is the design-space vector containing all the variables of concern which describe the machine's dimensions that need to be optimised.
  
  \section{MULTI-OBJECTIVE OPTIMISATION}
  A wide selection of multi-objective optimisation techniques are available, for example the Lexicographic Method, Bounded Objective Function Method, Normal Boundary Intersection Method, Normal Constraint Method, scalarisation (weighted) methods, and the Multi-objective Genetic Algorithm (MOGA) \cite{Marler2004} \cite{Oliveira2010} \cite{Das1998}. All the methods except for the last mentioned method, reduce the multi-objective optimisation problem to single-objective optimisation subproblems (where the weighted methods reduce the problem within a single step, and the other methods require sequential steps of single-objective solving or extra constraints to reduce the problem to a final single-objective problem). In contrast, MOGA can solve multi-objective problems directly and is also not gradient dependant, but is however more complex to program \cite{Marler2004}.
  
  No single method is best for all circumstances, therefore the method to be selected depends solely on the personal preferences and problem insight of the decision-maker \cite{Marler2004}, \cite{Oliveira2010}. In this study, the Weighted Sum Method (WSM) is used due to the ease of implementation and relative acceptable results. In spite of these advantages, much literature exists explaining the pitfalls of using this method. The first pitfall is that this method is unable to capture Pareto optimal points situated on non-convex portions of the Pareto optimal curve. The second pitfall is that the method does not produce an even distribution of points on the Pareto curve when using consistent change in weights \cite{Marler2010}. In \cite{Kim}, an adapted WSM method is revealed which determines uniformly-spaced Pareto optimal solutions, however this also adds more programming complexity and was avoided for this reason. The normal WSM is described as
  \begin{equation}\label{eq:3}
  f(\textbf{x}) = \sum_{k=1}^{n} \ w_{k}f_{k}(\textbf{x})~,
  \end{equation}
  where the weights (which can be selected otherwise) are chosen in this paper such that
  \begin{equation}\label{eq:5}
   \sum_{k=1}^{n} \ w_{k} = 1~.
  \end{equation}
  The multi-objective function \eqref{eq:2} is then scalarised into the form of
  \begin{equation}\label{eq:4}
  f(\textbf{x}) = w_{1}\eta(\textbf{x}) + w_{2}\tau(\textbf{x}) - w_{3}m(\textbf{x})~,
  \end{equation}
  where the weights $w_{1}$, $w_{2}$ and $w_{3}$ are varied to produce a representation of the Pareto optimal set. The last term is subtracted because it is desired to minimise PM mass, while maximising efficiency and torque.

  Furthermore, \cite{Marler2010} discussed that it can be difficult to distinguish between setting weights to compensate for differences in objective function magnitudes as opposed to setting weights in order to indicate the relative importance of an objective.  For this reason, it is advised to transform each objective function so that they all have similar magnitudes. Many different approaches exist to transform an objective function. One of the simplest, is to divide the objective function by its maximum achievable value \cite{Marler2004}, thereby the new transformed function has an upper limit of one and an unbounded lower limit. Practical realisable machines will not produce negative values of efficiency, torque or PM mass. The maximum value of each objective function can be obtained either by maximising only for the specific objective function or it can be determined by engineering intuition.
  \begin{equation}
   f_{i}^{\text{trans}}(\textbf{x}) = \frac{f(\textbf{x})}{|f_{i}^{\text{max}}(\textbf{x})|} 
  \end{equation}
 
  \section{IMPLEMENTATION OF OPTIMISATION}
  In order to insure that the weights operate as intended (indicating relative importance rather than compensating for difference in magnitudes), the objective functions in \eqref{eq:4} are normalised with their ideal values. The ideal values of each objective function are determined by setting only a single weight to one, and all other weights to zero. Then only a single objective function is optimised without competing against other objective functions.
  
  Figure \ref{fig:IDRFPM_design} shows the 9 design variables that are considered. All the variables are defined in per unit relative according to a single absolute value $r_{o}$ the outer radius. The optimisation process will realise that there is a mathematical relationship between variables $h_{mo}$, $g$, $h$, $h_{mi}$, $r_{i}$ and $r_{o}$, due to the fixed value of $r_{o}$ and the minimum value of $r_{i}$. The outer radius and stack length are kept constant so that it is possible to benchmark the optimised solutions with the existing prototype of IDRFPM machine. The air-gap width is also kept constant (equal to that of the prototype machine). The variable $p$ denotes the number of pole pairs, $k_{m}$ describes the ratio in width of the radially magnetised magnets compared to the tangentially magnetised magnets, whilst $k_{c}$ denotes the ratio of the coil core width compared to the coil slot width. Only non-overlapping coils are considered in this paper. The current density is kept equal to that of the working prototype, thereby hopefully ensuring feasible machines with regards to cooling of the coils.
  \begin{figure}
    \centering
    \includegraphics[width=0.43\textwidth]{simple_structure.pdf}
    \caption{IDRFPM with design variables}
    \label{fig:IDRFPM_design}
  \end{figure}
  
  \subsection{Single-Objective Optimisation}
  To optimise the single-objective function, an open-source Python-based package ``pyOpt'' was employed. Currently, only one of the methods in pyOpt are being used for this study, namely Sequential Penalty Derivative-free Methods for Nonlinear Constrained Optimisation (SDPEN). This method is ideally suited for engineering applications where the objective function are computed by simulation software and thus derivative information is unknown. The original problem is solved by a sequence of approximate minimisations of an augmented function of which constraint violations are progressively penalised \cite{Newton2010}. The 1-dimensional problem is solved using a line-search algorithm.
  
  \subsection{Overall Optimisation Process}
  The optimisation process to obtain the Pareto surface is described by the flow chart in Figure \ref{fig:my_flowchart3}. The simulation is achieved by an in-house 2D FEM package, named SEMFEM.
  
  \begin{figure}[h]
    \centering
    \includegraphics[width=0.43\textwidth]{my_flowchart3.pdf}
    \caption{Flowchart of optimisation process}
    \label{fig:my_flowchart3}
  \end{figure}  
  
  \section{OPTIMISATION RESULTS}
%   \lipsum[1-2]
  \begin{figure*}
    \includegraphics[trim=14cm 4cm 10cm 3cm,width=\textwidth]{figures/3d_plot11.pdf}
%     \fbox{\includegraphics[trim=14cm 4cm 10cm 3cm,width=\textwidth]{figures/3d_plot9.pdf}}
    \caption{3D Scatter plot of solution points approximating the Pareto front}
    \label{fig:3d_pareto}
  \end{figure*}
%   \lipsum[3-10]
  
%   \begin{figure}
%     \centering
% %     trim=left botm right top
% %     \fbox{\includegraphics[trim=12cm 2cm 4cm 2cm, width=0.53\textwidth]{figures/3d_plot4.pdf}}
%     \includegraphics[trim=12cm 2cm 4cm 2cm, width=1\textwidth]{figures/3d_plot5.pdf}
%     \caption{3D Plot of solutions obtained on the Pareto front}
%     \label{fig:3d_pareto}
%   \end{figure}  
  
  Figure \ref{fig:3d_pareto} shows the solutions that were obtained when the process of Figure \ref{fig:my_flowchart3} is executed. The weights are altered using an increment size of $0.025$, generating 861 solutions. The simulated output performance of the prototype is used as a benchmark. Any solutions, which in all aspects (efficiency, torque and PM mass), dominate the prototype machine's performance, are indicated with red dots. The prototype solution itself is indicated by the colour magenta. All other solutions are indicated with blue dots. From Figure \ref{fig:3d_pareto} it is clear that machines exist which dominate the prototype output performance.
  
  Table \ref{tab:dominating} shows the three solutions that were found, indicated by ``1'', ``2'', ``3'' which dominate the prototype machine, indicated by ``P''. For the purposes of the competition car, the gain in torque is probably the most appealing. Design solution 3 promises a slight reduction of the required PM mass ($2.97\%$), with unchanged efficiency, but a $15.58 \%$ increase in output torque.
  
  \begin{table}[h]
  \begin{tabular}{l | c | c | c | c}	%[l] = left justified column,[r] = right justified, [c] = centre column, to place lines between columns, use the "bar" symbol |  
   & P & 1 & 2 & 3\\
  \hline \hline
  Efficiency [\%] & 84.917 & 85.893 & 85.438 & 84.938\\
  Torque [Nm] & 22.644 & 25.611 & 24.601 & 26.172\\
  PM Mass [kg] & 2.488 & 2.469 & 2.309 & 2.414\\
  $w_{\eta}$ & - & 0.450 & 0.425 & 0.400\\
  $w_{\tau}$ & - & 0.250 & 0.250 & 0.275\\
  $w_{m}$ & - & 0.300 & 0.325 & 0.325\\
  \% $\eta$ increase & - & 1.150 & 0.613 & 0.024\\
  \% $\tau$ increase & - & 13.104 & 8.643 & 15.584\\
  \% $m$ increase & - & -0.775 & -7.188 & -2.973	%seperate column names with ampersand (&) and end last column with \\
  \end{tabular}
  \caption{Simulated solution results which dominate the simulated prototype design}
  \label{tab:dominating}
  \end{table}
  
%   Figures 1,2,3,4 illustrate the solutions as was tabulated in Table \ref{tab:dominating}. 
  The percentage change of the design variables compared to the prototype design are indicated by Table \ref{tab:variables}. As shown, variables $r_i$ and $k_{m}$ remain mostly unchanged. As mentioned before, $r_o$ is kept constant. It can also be seen that the general tendency is for the outer magnet thickness $h_{mo}$ to decrease and the inner magnet thickness $h_{mi}$ and coil thickness $h_c$ to increase. It is interesting to see that for all three solutions, the ratio between the coil core and coil winding angle $k_{c}$ settles at approximately $25 \%$ higher, and the number of pole pairs $43 \%$ (to $20$ pole pairs) higher, than the prototype value. In future optimisations, it can be considered to keep the variables $k_{c}$ and $p$ constant to the aforementioned values, thereby speeding up the optimisation process.
  
  \begin{table}[h]
  \begin{tabular}{l | r | c | c | c}	%[l] = left justified column,[r] = right justified, [c] = centre column, to place lines between columns, use the "bar" symbol |  
  Variables & P & $\Delta_{1} \%$ & $\Delta_{2} \%$ & $\Delta_{3} \%$\\
  \hline \hline
  $r_o$ & 131 \text{mm} & 0 & 0 & 0\\
  $r_i$ & 109 \text{mm} & -0.211 & 0.516 & -0.846\\
  $h_{mo}$ & 55 \text{mm} & -8.014 & -16.664 & -10.595\\
  $h_c$ & 9 \text{mm} & 2.950 & 2.387 & 12.904\\
  $h_{mi}$ & 55 \text{mm} & 7.378 & 0.188 & 6.103\\
  $k_{m}$ & 0.5 \text{p.u.} & -1.128 & -1.454 & -0.012\\
  $k_{c}$ & 0.718 \text{p.u.} & 24.737 & 24.419 & 26.447\\
  $p$ & 14 & 42.857 & 42.857 & 42.857

  \end{tabular}
  \caption{Design variable values (in \% change) of the solutions compared to the prototype design (in indicated units)}
  \label{tab:variables}
  \end{table}
  
%   Figure \ref{fig:design_solution} shows the resultant machine due to the third design solution. This figure can be compared with \label{fig:magnetic_path} which is identically sized to that of the prototype machine.
%   
%   \begin{figure}
%     \centering
%     %trim=left botm right top
%     \hbox{\includegraphics[trim=4cm 0cm 0cm 0cm, width=0.33\textwidth]{figures/design_sol.pdf}}
%     \caption{Drawing indicating design solution number 3}
%     \label{fig:design_solution}
%   \end{figure}
  
  \section{Eddy Current Losses}
  The IDRFPM design which is considered in this paper, does not contain any materials in the stator that can be easily magnetised. More specifically, the stator consists only of copper windings cast in resin, which both pertain similar magnetic permeabilities than that of air (also known as a coreless or air-cored stator). For this reason, the magnetic flux path produced by the inner and outer permanent magnets is equally spread in the stator; meaning the copper windings experience a similar magnitude of flux density than that of the winding core ``air''. This is a quality that is unique to coreless machines, and serves as motivation to investigate the severity of the eddy current losses for this particular machine. Once the approximate magnitude of the expected eddy losses is known, the designer can choose whether or not to include the eddy current loss calculations within the design optimisation process. For the purpose of the discussed optimisation process, it is desired to improve on the existing prototype. As mentioned earlier, the prototype machine is designed for a prototype car which will be entered in the Shell Eco Marathon competition. The competition restricts the maximum system voltage to $48 \text{V}$, therefore relatively large phase currents are required to deliver sufficient torque or power. For this reason, it is expected that conduction losses will be the most significant losses in the machine. It remains however important to investigate the eddy current losses when machines with different applications are envisioned.
  
  Eddy currents are produced in the stator windings when the machine rotates; the stator coils cut the rotating magnetic flux paths, thereby inducing a voltage on the coil terminals but also a voltage within each conductor, the latter voltage generates eddy currents. These eddy currents circulate in small loops perpendicular to the radial direction. The magnetic flux which the stator coil windings experience, are shown in Figure \ref{fig:magnetic_path}. The white regions in the stator indicate the air-cored areas of the coils. Notice that the flux moving through the stator are both in the radial and tangential directions, and that the flux paths are not affected by the relative position of the stator towards the rotors.
  
  \begin{figure}
    \centering
    \includegraphics[width=0.35\textwidth]{figures/band_machine_field001_aircored_flux(4).pdf}
    \caption{Conceptual IDRFPM machine with magnetic paths shown, sizing is identical to that of prototype machine}
    \label{fig:magnetic_path}
  \end{figure}  

  In \cite{Bossche2005}, analytical methods are categorized according to the particular situation. The methods are categorized according to low frequency, high frequency and wide frequency problems. The penetration depth is calculated in order to determine the appropriate method. The penetration depth is approximated by,
  \begin{equation}
  \delta = \sqrt{\frac{2 \rho_{c}}{\omega_{e} \mu}}
  \end{equation}
  where $\omega_{e}$ is the magnetic field's angular frequency experienced by the conductor. Here we use $\mu = \mu_{0}$ as copper has an almost equal permeability than that of free space, thus $\mu_{0}=1.25664 \times 10^{-6}$. The resistivity of the copper conductor is given by $\rho_{c} = 1.73 \times 10^{-8}$ $\Omega \text{m}$ at $25^{\circ} C$. As will be explained later, the resistivity at $25^{\circ} C$ is used to verify the theoretical calculation with measured results from the prototype. The rated speed of the 28 pole machine is 300 $\text{rpm}$ which equates to an electrical frequency of \varhidden{f}{70} $f_{e}=\SI{\calc{f}}{\hertz}$. The penetration depth is then \varhidden{u}{1.25664*10^{-6}} \varhidden{p}{1.73*10^{-8}*(1+0.0039*(25-25))} \varhidden{d}{1000*\sqrt{(2*p)/(2*\pi*70*u)}} $\delta = \num[round-precision = 4]{\calc{d}}$ \si{mm}.

  According to \cite{Bossche2005}, it is considered a low frequency situation when
  \begin{equation}
  d \le 1.6\delta
  \label{eq:lf}
  \end{equation}
  is satisfied. The conductor diameter is denoted by $d$. In a low frequency situation, the assumption is made that the eddy currents induced within the windings do not noticably change the applied magnetic field inside the conductor \cite{Bossche2006} \cite{Kamper2004}. This is the easiest route of determining the eddy current losses, if this is not the case then the eddy current induced magnetic field would also need to be taken into account.

  The prototype machine was initially constructed using $1.6$ \si{mm} before it was realized that the eddy current losses are too large. Consequently, a new stator was constructed which makes use of Litz wire. Each wire consists of 70 parallel strands, and a single strand has the diameter of $0.2$ \si{mm}. The measured eddy current results are shown in Table \ref{tab:eddy_measured}.

  \begin{table}[h]
  \centering
  \begin{tabular}{l | c | c }	%[l] = left justified column,[r] = right justified, [c] = centre column, to place lines between columns, use the "bar" symbol |  
  Conductor Diameter [\si{mm}] & Strands & $P_{eddy}$ [\si{W}]\\
  \hline \hline
  1.6  & 1 & 30.5\\
  0.2  & 70 & 0.6\\	
  %seperate column names with ampersand (&) and end last column with \\
  \end{tabular}
  \caption{Measured eddy current losses at 300 $\text{rpm}$ \cite{Oosthuizen2015a}}
  \label{tab:eddy_measured}
  \end{table}

  Equation \eqref{eq:lf} requires that the conductor diameter be less or equal than  $\num[round-precision = 4]{\calc{1.6*d}}$ \si{mm} to be qualified as a low frequency problem. Thus for both scenarios of Table \ref{tab:eddy_measured}, the magnetic field induced by the eddy currents can be neglected.

  Traditionally, the eddy current losses in windings for a low frequency situation are calculated using a classic equation derived by \cite{Carter1954}. The limitations are that it assumes only a one-dimensional (thus only in the radial direction) pure sinusoidal flux component. The classic approximation of the eddy current losses of a single conductor in a transverse alternating field is described by \cite{Carter1954} and \cite{Sullivan2004} as
  
    \begin{equation}
  P_{eddy}=\frac{\pi ld^4\hat{B}_{r_{1|r}}^2\omega_e^2}{128\rho_{c}}~, %\nonumber
  \label{eq:eddy0}
  \end{equation}  
  
  where $l$ is the axial length of the conductor, $\hat{B}_{r_{1|r}}$ is the peak value of the fundamental field density component in the radial direction (one-dimensional direction).
  
  Equation \eqref{eq:eddy0} can be used to account for all conductors within the machine

  \begin{equation}
  P_{eddy}=N\,Q\,n_{p}\left(\frac{\pi ld^4\hat{B}_{r_{1|r}}^2\omega_e^2}{64\rho_{c}}\right)~, %\nonumber
  \label{eq:eddy1}
  \end{equation}  

  where $N$ is the number of turns per coil per phase, $Q$ is the total (including all three phases) number  of stator coils, $n_{p}$ is the number of parallel strands per turn. A factor two is also multiplied to account for the return path of each conductor.

  A more accurate method is suggested by \cite{Kamper2004}. The method makes use of the classic equation \eqref{eq:eddy1}, and extends its use to accommodate for both radial and tangential flux components and does not assume pure sinusoidal flux components. The extra information of the field density is then obtained by a two-dimensional FEM simulation and subsequently using the Fast Fourier Transform. This method is therefore known as the hybrid method. Furthermore, it provides for different values of flux densities to be used depending on the position of the particular winding layer. This is important because the flux densities are fairly higher closer to the magnets and from \eqref{eq:eddy1} the eddy losses are proportional to $B^{2}$. The eddy current losses calculated using the hybrid method is described as

  \begin{equation}
  P_{eddy}=N\,Q\,n_{p}\left(\frac{\pi ld^4}{64\rho_{c}}\right) \sum_{j=1}^{l} n_{j}\ \sum_{i=1}^{n} (i\omega_e)^{2}\ \left( \hat{B}_{r_{ij}}^2 + \hat{B}_{t_{ij}}^2 \right) %\nonumber
  \label{eq:eddy2}
  \end{equation}  

  where $i$ denotes the harmonic number, $j$ is the layer number, footprint $r$ represents the radial component and footprint $t$ denotes the tangential component. The number of conductors in a specific layer is denoted by $n_{j}$. The reader is encouraged to read \cite{Kamper2004} for a more elaborative explanation on the hybrid method. 
  
  \subsection{Eddy Current Loss Results}
  The 2D magnetic field information was gathered at different layers using the previously mentioned SEMFEM package. Thereafter a Fast Fourier Transform was done to obtain the harmonic information. These values were then substituted into \eqref{eq:eddy2}. The prototype stator windings consist of 5 layers. It was decided that 15 harmonics is sufficient to capture the effect of the non-sinusoidal flux distribution in the machine's air gap. It should be mentioned that the the measured results of Table \ref{tab:eddy_measured} was obtained by an open-circuit test at the rated speed of 300 $\text{rpm}$. Because it is an open-circuit test, the machine temperature is fairly low. Therefore, to match the measured results, the copper resistivity was assumed to be the resistivity at $25^{\circ} C$. This produces slightly higher than usual eddy current losses. Table \ref{tab:eddy_results_combined} shows both measured and calculated losses.
  
  \begin{table}[h]
  \centering
  \begin{tabular}{l | c | c | c}	%[l] = left justified column,[r] = right justified, [c] = centre column, to place lines between columns, use the "bar" symbol |  
  $d$ [\si{mm}] & Strands & $P_{eddy}(1)$ [\si{W}] & $P_{eddy}(2)$ [\si{W}]\\
  \hline \hline
  1.6  & 1 & 30.5 & 27.58\\
  0.2  & 70 & 0.6 & 0.47\\	
  %seperate column names with ampersand (&) and end last column with \\
  \end{tabular}
  \caption{Measured $P_{eddy}(1)$ and calculated $P_{eddy}(2)$ eddy current losses at 300 $\text{rpm}$}
  \label{tab:eddy_results_combined}
  \end{table}
  
  The measured and calculated results match fairly well. The higher measured results could be attributed to wind and friction losses and eddy current losses at the end-windings. The prototype machine has a rated power of 700 $\si{W}$. It should be noted that the eddy losses for the 1.6 $\si{mm}$ conductors are then 4.36$\%$ of the machine's rated power.
  
%   The eddy current loss calculations are to be calculated for the final version of this paper. This will be compared with the eddy current losses measured by the prototype machine. It will also be shown, that the influence of the current flowing in the stator windings, minutely effects the resultant flux density in the stator. Other results, such as the overall efficiency and output torque are also to be measured from the prototype machine. These results will then serve as validation of the simulation results of the prototype contained within this paper. 
%   The eddy current losses are calculated using both equations (\ref{eq:eddy1}) and (\ref{eq:eddy2}) and are compared with the results measured by a prototype machine that was built and tested by \cite{Oosthuizen2015}.  
  
  \section{CONCLUSIONS}
  In this paper, it was desired to optimise an existing prototype machine, with emphasis on the motor efficiency, output torque and PM mass. The aforementioned objectives are usually competing objectives, and gives rise to the necessity of determining the Pareto surface. The basic theory behind multi-objective optimisation strategies was discussed and for this study, the Weighted Sum Method was introduced and implemented. The objective weights were varied such that the solution points approximate the 3-dimensional Pareto front. This allowed the decision-maker to select solutions which more accurately presents the desired preferences as compared to a priori articulation of preferences strategy. The optimisation process made use of the pyOpt open-source package to perform the single-objective optimisation. The electromagnetic simulation was performed using an in-house FEM package. The optimisation process considered 9 design variables, 2 of which were constant. The objective weights were varied according to a $0.025$ increment size, which generated 861 solutions. These solutions were plotted on a 3-dimensional scatter plot (see Figure \ref{fig:3d_pareto}). From this plot, the decision-maker can easier select and visualise solutions which satisfy the desired output performance. In this case, it was only desired to out-perform the present prototype design. The prototype's simulated efficiency, torque and PM mass was used as minimum requirements and only solutions which satisfied all three objectives were considered. Regarding the prototype car for the Shell Eco Marathon, it is most desired to increase the output torque. A machine was found (amongst two others), which promises approximately $3\%$ less PM mass, unchanged efficiency and a $15.58\%$ increase in torque.
  
  The theory for eddy current losses and the importance thereof in the IDRFPM machine was discussed. Two possible low frequency methods for approximating the eddy current losses were discussed. The first method is known as the classical analytical method and makes use of 1-dimensional flux density information and assumes a pure sinusoidal flux distribution. The second method extends the classical method to take into consideration 2-dimensional and non-pure sinusoidal flux density information. It also uses different flux densities depending on the position of the particular copper turn. This extra information is obtained via FEM simulation. The measured and calculated results matched fairly well. The slightly higher measured results can be attributed to wind and friction losses and eddy currents at the end-windings. It is clear that the use of Litz wire almost eliminates eddy current losses in the machine, thus it would not be necessary to include the eddy current losses in future optimisation processes. If however normal round copper wires are considered, the hybrid method ensures relatively accurate loss calculation which can then be included in the optimisation process.

  \section{FUTURE WORK}
  The coil mass can also be included in the optimisation process. At present, the design variable $k_m$ is assumed to be identical for both outer and inner magnets. An investigation can be done to see if the optimisation process would choose them differently.
  
  Currently, the peak flux density of the prototype in the air-gap is relatively low ($B_{pk} \approx 0.63 \text{T}$). This is mostly due to the fact that the air-cored stator adds high reluctance to the magnetic path produced by the magnets. An increase in magnetic flux density could result in an increase in torque and power density of the machine. Therefore, a future investigation could be done on the use of Soft Magnetic Composite (SMC) materials within the coil cores. Modern SMC materials promise relative magnetic permeabilities of up to $850$ and core losses as low as $6 \text{T/kg}$ at a $1 \text{T}$ flux density. However, the introduction of SMC materials possibly reintroduce other problems such as increased cogging torque and torque ripple, and an investigation of the stator layout would need to be done (the flux paths now depend on the relative position between the rotors and the stator, because the flux now favours moving through the SMC material).
  
  
  \section*{Acknowledgement}
    This work was supported by the Centre for Renewable and Sustainable Energy Studies (CRSES) and Stellenbosch University.
%hallo
% \bibliographystyle{IEEEtran}
\bibliographystyle{/home/andreas/texmf/tex/latex/IEEEtran/IEEEtran.bst}
\bibliography{/home/andreas/Bibliography/library.bib}

\end{document}