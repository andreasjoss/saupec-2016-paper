% my_flowchart.m4
% Here is a test file exercising the FlowchartDefs.m4 definitions
.PS
include(FlowchartDefs.m4)
include(pstricks.m4)
include(libcct.m4)
gen_init

  linethick_(1.0)
  arrowwid = 0.05
  arrowht = 0.1
  fillval = 0.85
  down

 
   
# Compound statement:
#[ right 
ellipse fill_(fillval) "Start"
arrow down linewid/2
x=4
y=4
fbox("Determine $f^{\text{max}}(\textbf{x})$ for $\eta$; $\tau$; $m$")# with .W at Here
arrow down linewid/2 from last [].S

fbox("Set $w_{\eta}=0$ and $w_{\tau}=1$")# with .W at Here
arrow right linewid/2 from last [].E

fbox("Set $w_{m}=0$")# with .W at Here
arrow down linewid/2 from last [].S

fifthenelse("$w_{m}=1$",,
  frepeatuntil(,,fbox("write new weights to input file")),
  fifthenelse("$A < B$",,
    fbox("Left"),
    fbox("Right")dnl
    )dnl
  ) with .N at Here

A: arrow down 0.25 from last [].S# ] with .nw at Case.sw + (0,-1.5)

#"\sl Compound statement" below at last [].A.end
.PE